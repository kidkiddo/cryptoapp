package main.crypto;

public class Morse implements Security {

    private static String[] MORSE = { ".- ", "-... ", "-.-. ", "-.. ", ". ",
            "..-. ", "--. ", ".... ", ".. ",

            ".--- ", "-.- ", ".-.. ", "-- ", "-. ", "--- ", ".--. ", "--.- ",
            ".-. ", "... ", "- ", "..- ",

            "...- ", ".-- ", "-..- ", "-.-- ", "--.. ", "| " };

    String input;

    public Morse(String input) {
        this.input = input;
    }

    public String encrypt() {
        StringBuilder result = new StringBuilder();
        String lowText = input.toLowerCase();
        for (int i = 0; i < input.length(); i++) {
            for (int j = 0; j < ALPHABET.length; j++) {
                if  (ALPHABET[j] == lowText.charAt(i)) {
                    result.append(MORSE[j]);
                }
            }
        }
        return result.toString();
    }

    public String decrypt() {
        return null;
    }
}
