package main.crypto;

public class ViceVersa implements Security {

    String input;

    public ViceVersa(String input) {
        this.input = input;
    }

    public String encrypt() {
        return new StringBuilder(input).reverse().toString();
    }

    public String decrypt() {
        return new StringBuilder(input).reverse().toString();
    }
}
