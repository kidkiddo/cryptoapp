package main.crypto;

import main.crypto.Security;



public class Cesar implements Security {
    private String input;
    private int key = 0;

    public Cesar(String line, int key) {
        setInput(line);
        this.key = key;
    }

    public Cesar(String line) {
        setInput(line);
    }

    public void setInput(String input) {
        this.input = input.toLowerCase();
    }

    //@Override
    public String decrypt() {
        return null;
    }

    //@Override
    public String encrypt() {
        int k;
        String out = "";
        for (int i = 0; i < this.input.length(); i++) {
            for (int j = 0; j < ALPHABET.length; j++) {
                if ((ALPHABET[j]) == (input.charAt(i))) {
                    k = j + key % ALPHABET.length;
                    out += ALPHABET[k];
                    break;
                }
            }
        }
        return out;
    }
}
