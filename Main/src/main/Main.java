package main;

import main.handler.HandlerImpl;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        HandlerImpl handler = new HandlerImpl();
        String line;

        while (!(line = reader.readLine()).equals("exit")) {
            handler.executeCommand(line);
        }
    }
}
