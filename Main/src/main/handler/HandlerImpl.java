package main.handler;

import main.crypto.Morse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HandlerImpl {

    public void executeCommand(String line) throws IOException {
        String input = "";
        if (line.startsWith("0")) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            StringBuilder fullString = new StringBuilder();
            while (!(input = reader.readLine()).equals("STOP")) {
                fullString.append(input);
            }
            System.out.println(new Morse(fullString.toString()).encrypt());
        }
        if (line.startsWith("1")) {
            String filePath = line.substring(1,line.length());
            BufferedReader reader = new BufferedReader(new FileReader(filePath));

            while ((input = reader.readLine()) != null) {
                //System.out.println(morse.encrypt(input));
            }
            //Morse morse = new Morse();
        }
    }
}
